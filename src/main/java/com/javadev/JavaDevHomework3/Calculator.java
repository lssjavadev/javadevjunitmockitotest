package com.javadev.JavaDevHomework3;

public class Calculator {

	public int add(int i, int j) {
		return i+j;
	}

	public int sub(int i, int j) {
		return i-j;
	}

	public int mult(int i, int j) {
		return i*j;
	}

	public double div(int i, int j) {
		return i/j;
	}

}
