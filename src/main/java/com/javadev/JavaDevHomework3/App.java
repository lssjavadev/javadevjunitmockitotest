package com.javadev.JavaDevHomework3;

public class App 
{
    public static void main( String[] args ) {
	    Calculator calc = new Calculator();
	    
        System.out.println(calc.add(2, 2));
        System.out.println(calc.sub(-5, -5));
        System.out.println(calc.mult(3, 2));
        System.out.println(calc.div(5, 2));
    }
}
