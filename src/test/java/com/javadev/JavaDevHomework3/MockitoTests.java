package com.javadev.JavaDevHomework3;

import static org.mockito.Mockito.*;

import org.junit.Test;

public class MockitoTests {
	
	@Test
	public void addingOppositeNumbersShouldReturnZero() {
	    Calculator tester = mock(Calculator.class);
	    
	    // Code;
	    tester.add(-0, 0);

	    tester.add(-2, 2);
	    tester.add(-2, 2);
	    
	    tester.add(-5, 5);
	    tester.add(-5, 5);
	    tester.add(-5, 5);

	    // Mockito tests;
	    when(tester.add(-0, 0)).thenReturn(0);
	    when(tester.add(-2, 2)).thenReturn(0);
	    when(tester.add(-5, 5)).thenReturn(0);
	    
	    // 1 time;
	    verify(tester).add(-0, 0);

	    // 2 times;
	    verify(tester, times(2)).add(-2, 2);

	    // 3 times;
	    verify(tester, times(3)).add(-5, 5);

	    // All calls overall - 6 times;
	    verify(tester, times(6)).add(anyInt(), anyInt());
	}


}
