package com.javadev.JavaDevHomework3;

import static org.junit.Assert.*;

import org.junit.Test;

public class JUnitTests {

	@Test
	public void addingOppositeNumbersShouldReturnZero() {
	    Calculator tester = new Calculator();

	    assertEquals("0 + -0 must be 0", 0, tester.add(0, -0));
	    assertEquals("2 + -2 must be 0", 0, tester.add(2, -2));
	    assertEquals("-5 + --5 must be 0", 0, tester.add(-5, -(-5)));
	}

	@Test
	public void subtractingSameNumbersShouldReturnZero() {
	    Calculator tester = new Calculator();

	    assertEquals("0 - 0 must be 0", 0, tester.sub(0, 0));
	    assertEquals("2 - 2 must be 0", 0, tester.sub(2, 2));
	    assertEquals("-5 - -5 must be 0", 0, tester.sub(-5, -5));
	}

	@Test
	public void multiplicationNumberByZeroShouldReturnZero() {
	    Calculator tester = new Calculator();

	    assertEquals("0 * 0 must be 0", 0, tester.mult(0, 0));
	    assertEquals("2 * 0 must be 0", 0, tester.mult(2, 0));
	    assertEquals("-5 * 0 must be 0", 0, tester.mult(-5, 0));
	}

	@Test
	public void dividingNumberByZeroShouldThrowException() {
	    Calculator tester = new Calculator();
	    
	    try {
		    tester.div(0, 0);
		    tester.div(2, 0);
		    tester.div(-5, 0);
		    
		    fail("There should be exception thrown!");
	    } catch(ArithmeticException e) {
	    	assertTrue(true);
	    }
	}

}
